
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source /usr/share/git/completion/git-completion.bash

# If user ID is greater than or equal to 1000 & if ~/bin exists and is a directory & if ~/.local/bin is not already in your $PATH
# then export ~/.local/bin to your $PATH.
if [[ $UID -ge 1000 && -d $HOME/.local/bin && -z $(echo $PATH | grep -o $HOME/.local/bin) ]]
then
    export PATH="${PATH}:$HOME/.local/bin"
fi

export PATH="$PATH:$HOME/.local/share/npm/bin"

alias ls='ls --color=auto'
alias weather='curl wttr.in/Baarn'
alias ll='ls -lah'


export PS1="\[$(tput setaf 6)\]\[$(tput bold)\]\[$(tput setaf 4)\]\u\[$(tput setaf 14)\]@\[$(tput setaf 4)\]\h \W\[$(tput setaf 2)\] λ: \[$(tput sgr0)\]"

export VISUAL="vim"
export EDITOR="$VISUAL"
export BROWSER="firefox"
export TERMINAL="st"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XINITRC="$XDG_CONFIG_HOME/X11"/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME/X11"/xserverrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GOPATH="$XDG_DATA_HOME"/go
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc 

export STEAM_RUNTIME=1

# ~

### Packages

| **Package** | **Description** | **Notes** | **Repository page** |
|-------------|-----------------|-----------|---------------------|
| i3 | window Manager | *gaps version* | AUR: [i3-gaps-next-git] |
| polybar | status bar | | AUR: [polybar-git] |
| rofi | application launcher | | Community: [rofi] |
| st | terminal | *.Xresources version* | AUR: [st-luke-git] |
| compton | compositor | *rounded corners version* | AUR: [compton-rounded-corners] |
| feh | image viewer | *and wallpaper setter* | Extra: [feh] |
| nemo | file manager | | Community: [nemo] |
| dunst | notification-daemon | | AUR: [dunst-git] |

[i3-gaps-next-git]: https://aur.archlinux.org/packages/i3-gaps-next-git/
[polybar-git]: https://aur.archlinux.org/packages/polybar-git/
[rofi]: https://www.archlinux.org/packages/community/x86_64/rofi/
[st-luke-git]: https://aur.archlinux.org/packages/st-luke-git/
[compton-rounded-corners]: https://aur.archlinux.org/packages/compton-rounded-corners/
[feh]: https://www.archlinux.org/packages/?name=feh
[nemo]: https://www.archlinux.org/packages/community/x86_64/nemo/
[dunst-git]: https://aur.archlinux.org/packages/dunst-git/

### Fonts

 - Fira Code (Community: [fira-code])
 - Font Awesome (AUR: [font-awesome])
 - System San Fransisco

[font-awesome]: https://aur.archlinux.org/ttf-font-awesome-4.git
[fira-code]: https://www.archlinux.org/packages/community/any/otf-fira-code/

### Styling

 - Theme: Raleigh [GTK2/3]
 - Icons: Papirus-Light [GTK2/3]

### Cloning

clone directly into home dir (if you are brave enough):
`git clone git@gitlab.com:DaanJacobs/dots.git ~/`

### Manual actions
- /etc/pacman.conf 
  - [sublime-text] Server = https:/download.sublimetext.com/arch/stable/x86_64
  - ILoveCandy


### TODO
- 

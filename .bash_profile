
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ -f ~/.bashrc ]] && . ~/.bashrc
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
 exec startx ~/.config/X11/xinitrc
fi

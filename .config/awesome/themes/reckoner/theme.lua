
local beautiful = require("beautiful")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local theme = {}

-- 0 8  black
-- 1 9  red
-- 2 10 green
-- 3 11 yellow
-- 4 12 blue 
-- 5 13 magenta
-- 6 14 cyan 
-- 7 15 white

theme.font          = "System San Fransisco 10"
theme.title_font    = "System San Fransisco 13"

-- C O L O R S --
local xrdb = beautiful.xresources.get_current_theme()

x = {
    background = xrdb.background ,
    foreground = xrdb.foreground ,
    color0     = xrdb.color0     ,
    color1     = xrdb.color1     ,
    color2     = xrdb.color2     ,
    color3     = xrdb.color3     ,
    color4     = xrdb.color4     ,
    color5     = xrdb.color5     ,
    color6     = xrdb.color6     ,
    color7     = xrdb.color7     ,
    color8     = xrdb.color8     ,
    color9     = xrdb.color9     ,
    color10    = xrdb.color10    ,
    color11    = xrdb.color11    ,
    color12    = xrdb.color12    ,
    color13    = xrdb.color13    ,
    color14    = xrdb.color14    ,
    color15    = xrdb.color15    ,
}

theme.primary 		= x.color8


theme.bg_normal     = x.background
theme.bg_focus      = x.color4
theme.bg_urgent     = x.color1
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = x.foreground
theme.fg_urgent     = x.foreground
theme.fg_minimize   = x.foreground

theme.useless_gap   = dpi(5)
theme.border_width  = dpi(5)
theme.border_normal = x.color4
theme.border_focus  = x.color1
theme.border_marked = x.color1


theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)


theme.titlebars_enabed = true
theme.titlebar_size = dpi(35)
theme.titlebar_title_enabled = false
theme.titlebar_font = "System San Fransisco 9"
theme.titlebar_title_align = "center"
theme.titlebar_position = "left"
theme.titlebar_bg = "#111111"
theme.titlebar_fg_focus = x.background
theme.titlebar_fg_normal = x.color8


-- Wibar(s)
theme.wibar_height = dpi(55)
theme.wibar_width = dpi(55)
theme.wibar_fg = x.foreground
theme.wibar_bg = x.background
theme.prefix_fg = x.color8


-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

theme.wallpaper = "/home/djcbs/Pictures/walls/bg-gradient-bluegreen.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"


-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

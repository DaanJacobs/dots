-- R C  E N T R Y P O I N T --

local themes = {
  "reckoner"
}

local theme = themes[1]


require("configuration.client")
local buttons = require("configuration.buttons")


-- U S E R  P R E F E R E N C E S --
user = {
  terminal = "st",
  floating_terminal = "st",
  profile_picture = os.getenv("HOME").."/.config/awesome/profile.png",
  screenshot_dir = os.getenv("HOME") .. "/Pictures/Screenshots/"
}



-- T H E M I N G --
local beautiful = require("beautiful")
local theme_dir = os.getenv("HOME") .. "/.config/awesome/themes/" .. theme .. "/"
beautiful.init(theme_dir .. "theme.lua")
dpi = beautiful.xresources.apply_dpi

local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

local naughty = require("naughty")

-- naughty.connect_signal("request::display_error", function(message, startup)
--     naughty.notification {
--         urgency = "critical",
--         title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
--         message = message
--     }
-- end)



-- R I G H T  C L I C K  M E N U --
myawesomemenu = {
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end }
}

mymainmenu = awful.menu(
  { items = { 
    { "awesome", myawesomemenu, beautiful.awesome_icon },
    { "open terminal", user.terminal }
  }
})



-- W A L L P A P E R --
screen_width = awful.screen.focused().geometry.width
screen_height = awful.screen.focused().geometry.height

local function set_wallpaper(s)
    if beautiful.wallpaper then
        awful.spawn.with_shell(os.getenv("HOME") .. "/.fehbg")
    end
end

awful.screen.connect_for_each_screen(function(s)
    set_wallpaper(s)


end)

screen.connect_signal("property::geometry", set_wallpaper)


require("configuration.tags")


-- Determines how floating clients should be placed
local client_placement_f = awful.placement.no_overlap + awful.placement.no_offscreen



-- S I G N A L S --

_G.client.connect_signal('manage', function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not _G.awesome.startup then
      awful.client.setslave(c)
    end

    if _G.awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
      -- Prevent clients from being unreachable after screen count changes.
      awful.placement.no_offscreen(c)
    end
  end
)

-- Enable sloppy focus, so that focus follows mouse.

_G.client.connect_signal('mouse::enter', function(c)
    c:emit_signal('request::activate', 'mouse_enter', {raise = true})
  end
)

_G.client.connect_signal('focus', function(c)
    c.border_color = beautiful.border_focus
  end
)
_G.client.connect_signal('unfocus', function(c)
    c.border_color = beautiful.border_normal
  end
)

-- Center client when floating property changes (a-la i3)
client.connect_signal("property::floating", function(c)
    awful.placement.centered(c, {honor_padding = true, honor_workarea=true})
  end
)


require('configuration.titlebar')


-- S T A R T U P  A P P S --
awful.spawn.with_shell( os.getenv("HOME") .. "/.config/awesome/autostart.sh")



-- G A R B A G E  C O L L E C T I O N --
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)

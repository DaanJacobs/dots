#!/usr/bin/bash

function run {
    if ! pgrep $1 > /dev/null ;
    then
        $@&
    fi
}

xrdb ~/.Xresources

run compton --config ~/.config/picom/picom.conf

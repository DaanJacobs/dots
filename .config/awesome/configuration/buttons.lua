-- M O U S E  B U T T O N S --

local awful = require('awful')
local gears = require('gears')

modkey = "Mod4"

buttons = {}



-- D E S K T O P  B U T T O N S --
buttons.desktopbuttons = gears.table.join(
    -- Right click
    awful.button({ }, 3, function () mymainmenu:toggle() end)
)

-- Mouse buttons on the tasklist
buttons.tasklistbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            -- Without this, the following
            -- :isvisible() makes no sense
            c.minimized = false
            if not c:isvisible() and c.first_tag then
                c.first_tag:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
        end
    end),
    -- Middle mouse button closes the window
    awful.button({ }, 2, function (c) 
        c:kill() 
    end),
    awful.button({ }, 3, function (c) 
        c.minimized = true 
    end),
    awful.button({ }, 4, function ()
        awful.client.focus.byidx(-1)
    end),
    awful.button({ }, 5, function ()
        awful.client.focus.byidx(1)
    end),

    -- Side button up - toggle floating
    awful.button({ }, 9, function(c)
        -- c:raise()
        c.floating = not c.floating
    end),
    -- Side button down - toggle ontop
    awful.button({ }, 8, function()
        -- c:raise()
        c.ontop = not c.ontop
    end)
)



-- T A G L I S T  B U T T O N S --
buttons.taglistbuttons = gears.table.join(
    awful.button({ }, 1, function(t)
        -- t:view_only()
        helpers.tag_back_and_forth(t.index)
    end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    -- awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ }, 3, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({ }, 4, function(t) 
        awful.tag.viewprev(t.screen) 
    end),
    awful.button({ }, 5, function(t) 
        awful.tag.viewnext(t.screen) 
    end)
)



-- T I T L E B A R  B U T T O N S --
buttons.titlebarbuttons = gears.table.join(
    awful.button({ }, 1, function()
        c:emit_signal("request::activate", "titlebar", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ }, 3, function()
        c:emit_signal("request::activate", "titlebar", {raise = true})
        awful.mouse.client.resize(c)
    end)
)



-- C L I E N T  B U T T O N S --
buttons.clientbuttons = awful.util.table.join(
  awful.button({}, 1, function(c)
      _G.client.focus = c
      c:raise()
    end),
  awful.button({modkey}, 1, awful.mouse.client.move),
  awful.button({modkey}, 3, awful.mouse.client.resize)
)

root.buttons(buttons.desktopbuttons)

return buttons

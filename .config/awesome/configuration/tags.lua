
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local icons = require("icons")
local search_button = require("widgets.search")
local TagList = require("widgets.taglist")



-- L A Y O U T S --

awful.layout.layouts = {
  awful.layout.suit.tile,
  awful.layout.suit.max
}



-- T A G S --

local tags = {
  {
    icon = icons.terminal,
    type = 'terminal',
    defaultApp = 'st',
    screen = 1,
    name = '1',
  },
  {
    icon = icons.firefox,
    type = 'browser',
    defaultApp = 'firefox',
    screen = 1,
    name = '2',
  },
  {
    icon = icons.sublime,
    type = 'editor',
    defaultApp = 'sublime',
    screen = 1,
    name = '3',
  },
  {
    icon = icons.steam,
    type = 'games',
    defaultApp = 'steam',
    screen = 1,
    name = '4',
  },
  {
    icon = icons.spotify,
    type = 'music',
    defaultApp = 'spotify',
    screen = 1,
    name = '5',
  },
  {
  icon = icons.six,
    type = 'any',
    defaultApp = '',
    screen = 1,
    name = '6',
  },
  {
    icon = icons.seven,
    type = 'any',
    defaultApp = '',
    screen = 1,
    name = '7',
  },
  {
    icon = icons.eight,
    type = 'any',
    defaultApp = '',
    screen = 1,
    name = '8',
  },
  {
    icon = icons.nine,
    type = 'any',
    defaultApp = '',
    screen = 1,
    name = '9',
  },
  {
    icon = icons.discord,
    type = 'chat',
    defaultApp = 'discord',
    screen = 1,
    name = '10',
  }
}

awful.screen.connect_for_each_screen(
  function(s)
    for i, tag in pairs(tags) do
      awful.tag.add(
        tags[i].name,
        {
          icon = tag.icon,
          icon_only = false,
          layout = awful.layout.suit.tile,
          gap_single_client = true,
          gap = 5,
          screen = tag.screen,
          defaultApp = tag.defaultApp,
          selected = i == 1
        }
      )
    end
  end
)


-- TODO move to widgets?
local textclock = wibox.widget.textclock('<span font="Fira Code bold 11">%H\n%M</span>')
local clock_widget = wibox.container.margin(textclock, dpi(13), dpi(13), dpi(8), dpi(8))

awful.screen.connect_for_each_screen(function(s)
    
    s.search_button = search_button()

    -- Top menu
    s.topmenu = awful.wibar({ position = "top", screen = s })
    s.topmenu:setup {
        layout = wibox.layout.fixed.horizontal,
        s.search_button,
    }

    -- Left menu
    s.leftmenu = awful.wibar({ position = "left", screen = s })
    s.leftmenu:setup {
        layout = wibox.layout.align.vertical,
        { 
          layout = wibox.layout.fixed.vertical,
          TagList(s),
            
        },
        nil,
        {
          layout = wibox.layout.fixed.vertical,
          clock_widget,
        }
    }


end)

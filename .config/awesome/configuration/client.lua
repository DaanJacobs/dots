-- C L I E N T --

local awful = require('awful')
local gears = require('gears')
local keys = require('configuration.keys')
local buttons = require('configuration.buttons')



-- R U L E S --
awful.rules.rules = {
  -- A L L --
  {
    rule = {},
    properties = {
      focus = awful.client.focus.filter,
      raise = true,
      titlebars_enabled = false,
      keys = keys.clientkeys,
      buttons = buttons.clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_offscreen,
      floating = false,
      maximized = false,
      above = false,
      below = false,
      ontop = false,
      sticky = false,
      maximized_horizontal = false,
      maximized_vertical = false,
      shape = function()
        return function(cr, w, h)
          gears.shape.rounded_rect(cr, w, h, 8)
        end
      end
    }
  },
  

  -- D I A L O G --
  {
    rule_any = {type = {'dialog'}},
    properties = {
      placement = awful.placement.centered,
      ontop = true,
      floating = true,
      drawBackdrop = true,
      skip_decoration = true
    }
  },

  -- P R O G R A M  R U L E S --
  {
    rule_any   = { 
      class = {
        "firefox"
      }
    },
    properties = { 
      tag = '2'
    }
  },

  {
    rule_any   = { 
      class = {
        "Sublime_text"
      }
    },
    properties = { 
      tag = '3'
    }
  },

  {
    rule_any   = { 
      class = {
        "discord"
      }
    },
    properties = { 
      tag = '10'
    }
  },

 {
    rule_any   = { 
      class = {
        "Spotify"
      }
    },
    properties = { 
      tag = '5'
    }
  }

}

-- T I T L E B A R --
 
 local awful = require("awful")
 local beautiful = require("beautiful")

local gen_button, gen_button_size, gen_button_margin
local gen_button_size = dpi(8)
local gen_button_margin = dpi(8)
local gen_button_color_unfocused = x.color8
gen_button = function (c, color, hover_color, cmd)
    local button = wibox.widget {
        forced_height = gen_button_size,
        forced_width = gen_button_size,
               bg = gen_button_color_unfocused,
               shape = gears.shape.circle,
        widget = wibox.container.background()
    }

       local button_widget = wibox.widget {
        button,
        margins = gen_button_margin,
        widget = wibox.container.margin(),
    }
    button_widget:buttons(gears.table.join(
            awful.button({ }, 1, function ()
                cmd(c)
            end)
        ))

       button_widget:connect_signal("mouse::enter", function () 
        button.bg = hover_color
    end)
    button_widget:connect_signal("mouse::leave", function () 
        if c == client.focus then
            button.bg = color
        else
            button.bg = gen_button_color_unfocused
        end
    end)



       c:connect_signal("focus", function ()
        button.bg = color
           end)
    c:connect_signal("unfocus", function ()
        button.bg = gen_button_color_unfocused
           end)

    return button_widget
end


client.connect_signal("request::titlebars", function(c)
  
  awful.titlebar(c, {position = beautiful.titlebar_position}) : setup {
      {
        {
          gen_button(c, x.color3, x.color11, window_minimize),
          gen_button(c, x.color2, x.color10, window_maximize),
          spacing = dpi(7),
          layout  = wibox.layout.fixed.vertical
        },
        margins = dpi(10),
        widget = wibox.container.margin
      },
      {
        buttons = buttons,
        layout = wibox.layout.flex.vertical
      },

    layout = wibox.layout.align.vertical
  }
    
end)

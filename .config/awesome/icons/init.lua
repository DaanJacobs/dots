
local dir = os.getenv('HOME') .. '/.config/awesome/icons/'


return {

	search   = dir .. 'search.png',

	terminal = dir .. 'terminal.svg',
	firefox  = dir .. 'firefox.svg',
	sublime  = dir .. 'sublime.svg',
	steam    = dir .. 'steam.svg',
	spotify  = dir .. 'spotify.svg',
	six      = dir .. 'six.svg',
	seven    = dir .. 'seven.svg',
	eight    = dir .. 'eight.svg',
	nine     = dir .. 'nine.svg',
	discord  = dir .. 'discord.svg',

}
